import {autoComView} from './autocom-view';

export default class AutoCom {
  constructor(config) {
    const {
      selector = '#autocom-field',
      url = '',
      data: {
        src,
        keyPath
      },
      propertySearch,
      propertyValue,
      characters = 2,
      isCleared = false
    } = config;

    this.selector = selector;
    this.url = url;
    this.data = {
      src: () => (typeof src == 'function' ? src() : src),
      keyPath
    };
    this.propertySearch = propertySearch;
    this.propertyValue = propertyValue;
    this.characters = characters;
    this.isCleared = isCleared;
    this.arrayMatches = [];
    this.input = autoComView.getInput(this.selector);
    this.inputHidden = autoComView.createInputHidden();
    this.list = autoComView.createList(this.input);

    this.init();
  }

  searchMatches(query) {
    this.arrayMatches = [];
    const pattern = new RegExp(`${query}`, 'gi');
    this.dataStream.forEach(item => {
      if (item[this.propertySearch].search(pattern) === 0) {
        this.arrayMatches.push({value: item[this.propertyValue], content: item[this.propertySearch]});
      }
    })
  }

  handleInput(event) {
    const value = event.target.value;
    if (value.length < this.characters) {
      autoComView.clearList(this.list);
      return;
    }
    this.searchMatches(value);
    autoComView.addListItemToList(this.arrayMatches, this.list);
    autoComView.navigation(this.input, this.list);
  }

  init() {
    Promise.resolve(this.data.src()).then(data => {
      this.dataStream = this.data.keyPath ? data[this.data.keyPath] : data;
    });
    autoComView.buildStructure(this.input, this.inputHidden, this.list, this.isCleared);
    this.input.addEventListener('input', this.handleInput.bind(this));
  }
}
