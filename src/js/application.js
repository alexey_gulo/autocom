import AutoCom from './autocom';

const url = 'https://raw.githubusercontent.com/aZolo77/citiesBase/master/cities.json';

new AutoCom({
  selector: '#input',
  url: url,
  data: {
    src: async () => {
      const source = await fetch(url);
      return await source.json();
    },
    keyPath: 'city'
  },
  characters: 2,
  isCleared: false,
  propertySearch: 'name',
  propertyValue: 'city_id'
});
